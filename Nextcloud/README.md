# Nextcloud

On va commencer par configurer les DNS :
```bash
sudo vi /etc/resolv.conf
```

On rajoute une ligne juste avant celle contenant `nameserver` :
```bash
[...]
nameserver 192.168.56.103 # IP serveur DNS local
[...]
```

On va ensuite rendre le fichier *imutable* pour qu'il ne soit pas modifié :
```bash
sudo chattr +i /etc/resolv.conf
```

On modifie aussi notre FQDN dans le fichier `/etc/hostname` :
```bash
nextcloud.projetinfra.net
```

## Installation de nginx et php7-fpm

Pour installer nginx, il faut d'abord installer les dépôts source du package EPEL :
```bash
sudo yum -y install epel-release
sudo yum -y install nginx
```
Pareil pour php7-fpm, il faut installer le dépôts webtatic :
```bash
sudo rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm
sudo yum -y install php72w-fpm
```
On va également installer plusieurs modules php nécessaires au fonctionnement de Nextcloud :
```bash
sudo yum -y install php72w-cli php72w-mysql php72w-intl php72w-xml php72w-mbstring php72w-gd
```

## Configurer php7-fpm

On va modifier le fichier de configuration par défaut de php-fpm :
```bash
sudo vi /etc/php-fpm.d/www.conf
```
Et modifier ces lignes :
```bash
user=nginx
group=nginx

listen=127.0.0.1:9000

env[HOSTNAME] = $HOSTNAME
env[PATH] = /usr/local/bin:/usr/bin:/bin
env[TMP] = /tmp
env[TMPDIR] = /tmp
env[TEMP] = /tmp
```

On peut désormais démarrer nginx et php-fpm :
```bash
sudo systemctl start nginx && sudo systemctl enable nginx
sudo systemctl start php-fpm && sudo systemctl enable php-fpm
```

## Installer et configurer MariaDB

Pour l'installer, on va simplement :
```bash
sudo yum -y install mariadb-server
```
Puis on le démarre :
```bash 
sudo systemctl start mariadb && sudo systemctl enable mariadb
```
On configure le mot de passe pour root avec `mysql_secure_installation`. Tapez 'Y' et 'Entrée' à chaque étape.  
Puis on se connecte :
```bash
mysql -u root -p
```
Rentrez votre mot de passe et on va créer une base de données et un utilisateur pour Nextcloud :
```sql
CREATE DATABASE nextcloud_db;
CREATE USER 'nextclouduser'@'localhost' IDENTIFIED BY 'mot_de_passe_nextcloud';
GRANT ALL PRIVILEGES ON nextcloud_db.* TO 'nextclouduser'@'localhost';
FLUSH PRIVILEGES;
```

## Générer un certificat SSL auto-signé 

On va générer un certificat avec `openssl` pour Nextcloud :
```bash 
sudo mkdir /etc/nginx/cert
sudo openssl req -new -x509 -days 365 -nodes -out /etc/nginx/cert/nextcloud.crt -keyout /etc/nginx/cert/nextcloud.key
```

## Télécharger et installer Nextcloud

On va avoir besoin de `wget` et `unzip` :
```bash
sudo yum -y install wget unzip
```
On va télécharger Nextcloud dans `/tmp` puis déplacer le tout dézippé dans `/usr/share/nginx/html` :
```bash
cd /tmp
wget https://download.nextcloud.com/server/releases/nextcloud-17.0.3.zip
unzip nextcloud-17.0.3.zip
sudo mv nextcloud/ /usr/share/nginx/html/ 
```
On termine en créer un dossier `data` pour nextcloud :
```bash
cd /usr/share/nginx/html
mkdir nextcloud/data
sudo chown -R nginx:nginx nextcloud/
```

## Configurer le virtual host pour nextcloud dans nginx

On va donc créer un nouveau fichier de configuration :
```bash
sudo vi /etc/nginx/conf.d/nextcloud.conf
```
En suivant la [documentation](https://docs.nextcloud.com/server/17/admin_manual/installation/nginx.html), voilà ce qu'on écrit dedans :
```bash
upstream php-handler {
    server 127.0.0.1:9000;
    #server unix:/var/run/php/php7.2-fpm.sock;
}

server {
    listen 80;
    listen [::]:80;
    server_name booexcloud.net;
    # enforce https
    return 301 https://$server_name:443$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name booexcloud.net;

    # Use Mozilla's guidelines for SSL/TLS settings
    # https://mozilla.github.io/server-side-tls/ssl-config-generator/
    # NOTE: some settings below might be redundant
    ssl_certificate /etc/nginx/cert/nextcloud.crt;
    ssl_certificate_key /etc/nginx/cert/nextcloud.key;

    # Add headers to serve security related headers
    # Before enabling Strict-Transport-Security headers please read into this
    # topic first.
    #add_header Strict-Transport-Security "max-age=15768000; includeSubDomains; preload;" always;
    #
    # WARNING: Only add the preload option once you read about
    # the consequences in https://hstspreload.org/. This option
    # will add the domain to a hardcoded list that is shipped
    # in all major browsers and getting removed from this list
    # could take several months.
    add_header Referrer-Policy "no-referrer" always;
    add_header X-Content-Type-Options "nosniff" always;
    add_header X-Download-Options "noopen" always;
    add_header X-Frame-Options "SAMEORIGIN" always;
    add_header X-Permitted-Cross-Domain-Policies "none" always;
    add_header X-Robots-Tag "none" always;
    add_header X-XSS-Protection "1; mode=block" always;

    # Remove X-Powered-By, which is an information leak
    fastcgi_hide_header X-Powered-By;

    # Path to the root of your installation
    root /var/www/nextcloud;

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

    # The following 2 rules are only needed for the user_webfinger app.
    # Uncomment it if you're planning to use this app.
    #rewrite ^/.well-known/host-meta /public.php?service=host-meta last;
    #rewrite ^/.well-known/host-meta.json /public.php?service=host-meta-json last;

    # The following rule is only needed for the Social app.
    # Uncomment it if you're planning to use this app.
    #rewrite ^/.well-known/webfinger /public.php?service=webfinger last;

    location = /.well-known/carddav {
      return 301 $scheme://$host:$server_port/remote.php/dav;
    }
    location = /.well-known/caldav {
      return 301 $scheme://$host:$server_port/remote.php/dav;
    }

    # set max upload size
    client_max_body_size 512M;
    fastcgi_buffers 64 4K;

    # Enable gzip but do not remove ETag headers
    gzip on;
    gzip_vary on;
    gzip_comp_level 4;
    gzip_min_length 256;
    gzip_proxied expired no-cache no-store private no_last_modified no_etag auth;
    gzip_types application/atom+xml application/javascript application/json application/ld+json application/manifest+json application/rss+xml application/vnd.geo+json application/vnd.ms-fontobject application/x-font-ttf application/x-web-app-manifest+json application/xhtml+xml application/xml font/opentype image/bmp image/svg+xml image/x-icon text/cache-manifest text/css text/plain text/vcard text/vnd.rim.location.xloc text/vtt text/x-component text/x-cross-domain-policy;

    # Uncomment if your server is build with the ngx_pagespeed module
    # This module is currently not supported.
    #pagespeed off;

    location / {
        rewrite ^ /index.php;
    }

    location ~ ^\/(?:build|tests|config|lib|3rdparty|templates|data)\/ {
        deny all;
    }
    location ~ ^\/(?:\.|autotest|occ|issue|indie|db_|console) {
        deny all;
    }

    location ~ ^\/(?:index|remote|public|cron|core\/ajax\/update|status|ocs\/v[12]|updater\/.+|oc[ms]-provider\/.+)\.php(?:$|\/) {
        fastcgi_split_path_info ^(.+?\.php)(\/.*|)$;
        set $path_info $fastcgi_path_info;
        try_files $fastcgi_script_name =404;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $path_info;
        fastcgi_param HTTPS on;
        # Avoid sending the security headers twice
        fastcgi_param modHeadersAvailable true;
        # Enable pretty urls
        fastcgi_param front_controller_active true;
        fastcgi_pass php-handler;
        fastcgi_intercept_errors on;
        fastcgi_request_buffering off;
    }

    location ~ ^\/(?:updater|oc[ms]-provider)(?:$|\/) {
        try_files $uri/ =404;
        index index.php;
    }

    # Adding the cache control header for js, css and map files
    # Make sure it is BELOW the PHP block
    location ~ \.(?:css|js|woff2?|svg|gif|map)$ {
        try_files $uri /index.php$request_uri;
        add_header Cache-Control "public, max-age=15778463";
        # Add headers to serve security related headers (It is intended to
        # have those duplicated to the ones above)
        # Before enabling Strict-Transport-Security headers please read into
        # this topic first.
        #add_header Strict-Transport-Security "max-age=15768000; includeSubDomains; preload;" always;
        #
        # WARNING: Only add the preload option once you read about
        # the consequences in https://hstspreload.org/. This option
        # will add the domain to a hardcoded list that is shipped
        # in all major browsers and getting removed from this list
        # could take several months.
        add_header Referrer-Policy "no-referrer" always;
        add_header X-Content-Type-Options "nosniff" always;
        add_header X-Download-Options "noopen" always;
        add_header X-Frame-Options "SAMEORIGIN" always;
        add_header X-Permitted-Cross-Domain-Policies "none" always;
        add_header X-Robots-Tag "none" always;
        add_header X-XSS-Protection "1; mode=block" always;

        # Optional: Don't log access to assets
        access_log off;
    }

    location ~ \.(?:png|html|ttf|ico|jpg|jpeg|bcmap)$ {
        try_files $uri /index.php$request_uri;
        # Optional: Don't log access to other assets
        access_log off;
    }
}
```

Testez la configuration avec :
```bash
sudo nginx -t
```
S'il n'y a pas d'erreur :
```bash
sudo systemctl restart nginx
```

## Configurer SELinux et le firewall

On va mettre SELinux en *permissive* :
```bash
sudo setenforce 0
sudo vi /etc/selinux/config # SELINUX=enforcing -> SELINUX=permissive
```
Pour vérifier : `sestatus`  

Pour le firewall, on va autoriser les ports 80 et 443 :
```bash
# S'il n'est pas déjà actif
sudo systemctl start firewalld && sudo systemctl enable firewalld

sudo firewall-cmd --permanent --add-port=80/tcp
sudo firewall-cmd --permanent --add-port=443/tcp

sudo firewall-cmd --reload
```

À ce stade on devrait pouvoir accèder à Nextcloud depuis notre navigateur.

## Première connexion

Il faut maintenant renseigner un nom d'utilisateur et un mot de passe pour l'admin.  
Il faut également sélectionner le dossier où seront stockées les données Nextcloud et renseigner les identifiants pour se connecter à MariaDB.

![First connection](./img/first_connexion_NC.png)