# Serveur DNS

On va commencer par télécharger le serveur DNS `bind` et les utilitaires DNS `bind-utils` :
```bash
sudo yum -y install bind bind-utils
```
Une fois cela fait, on va configurer le service `named` :
```bash
sudo vi /etc/named.conf
```
Voilà ce qu'il doit contenir :
```bash
options {
        listen-on port 53 { 127.0.0.1; 192.168.56.103; }; ### DNS primaire ###
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        recursing-file  "/var/named/data/named.recursing";
        secroots-file   "/var/named/data/named.secroots";
        allow-query     { localhost; 192.168.56.0/24; }; ### Fourchette d'IP ###

        /*
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable
           recursion.
         - If your recursive DNS server has a public IP address, you MUST enable access
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface
        */
        recursion yes;

        dnssec-enable yes;
        dnssec-validation yes;

        /* Path to ISC DLV key */
        bindkeys-file "/etc/named.root.key";

        managed-keys-directory "/var/named/dynamic";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
        type hint;
        file "named.ca";
};

zone "projetinfra.net" IN { ### Forward ###
        type master;
        file "forward.projetinfra";
        allow-update { none; };
};

zone "56.168.192.in-addr.arpa" IN { ### Reverse ###
        type master;
        file "reverse.projetinfra";
        allow-update { none; };
};

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```
On créer ensuite le fichier de zone :
```bash
sudo vi /var/named/forward.projetinfra
```
Et écrire dedans :
```bash
$TTL 86400
@   IN  SOA     masterdns.projetinfra.net. root.projetinfra.net. (
        2011071001  ;Serial
        3600        ;Refresh
        1800        ;Retry
        604800      ;Expire
        86400       ;Minimum TTL
)
@       IN  NS          masterdns.projetinfra.net.
@       IN  A           192.168.56.101
@       IN  A           192.168.56.102
@       IN  A           192.168.56.103
@       IN  A           192.168.56.104
nextcloud       IN  A   192.168.56.101
borgreplica     IN  A   192.168.56.102
masterdns       IN  A   192.168.56.103
freeipa         IN  A   192.168.56.104
```

On créer un fichier de zone inverse :
```bash
sudo vi /var/named/reverse.projetinfra
```
Dans lequel on écrit :
```bash
$TTL 86400
@   IN  SOA     masterdns.projetinfra.net. root.projetinfra.net. (
        2011071001  ;Serial
        3600        ;Refresh
        1800        ;Retry
        604800      ;Expire
        86400       ;Minimum TTL
)
@       IN  NS          masterdns.projetinfra.net.
@       IN  PTR         projetinfra.net.
masterdns       IN  A   192.168.56.103
borgreplica     IN  A   192.168.56.102
nextcloud       IN  A   192.168.56.101
freeipa         IN  A   192.168.56.104
103    IN  PTR         masterdns.projetinfra.net.
102    IN  PTR         borgreplica.projetinfra.net.
101    IN  PTR         nextcloud.projetinfra.net.
104    IN  PTR         freeipa.projetinfra.net
```

On peut désormais démarrer le service `named` et l'activer au démarrage :
```bash
sudo systemctl start named && sudo systemctl enable named
```

On autorise le service DNS à communiquer avec l'extérieur :
```bash
sudo firewall-cmd --permanent --add-port=53/tcp
sudo firewall-cmd --permanent --add-port=53/udp

sudo firewall-cmd --reload
```

Et on n'oublie pas de mettre SELinux en mode *permissive* :
```bash
sudo setenforce 0
sudo vi /etc/selinux/config #SELINUX=enforcing -> SELINUX=permissive

# Pour vérifier
sestatus
```

Configuration des permissions :
```bash
sudo chgrp named -R /var/named
sudo chown -v root:named /etc/named.conf
sudo restorecon -rv /var/named
sudo restorecon /etc/named.conf
```

Vérification du fichier de configuration :
```bash
sudo named-checkconf /etc/named.conf
```

Vérification de la zone :
```bash
sudo named-checkzone projetinfra.local /var/named/forward.projetinfra
```

Vérification de la zone inverse :
```bash
sudo named-checkzone projetinfra.local /var/named/reverse.projetinfra 
```

Configuration de l'interface réseau :
```bash
sudo vi /etc/sysconfig/network-scripts/ifcfg-enp0s8
```
Et rajouter cette ligne à la fin :
```bash
DNS="192.168.56.103"
```

On va ajouter une ligne dans le fichier `/etc/resolv.conf` juste au dessus de la ligne contenant *nameserver* et on rend le fichier *imutable* pour qu'il ne soit pas modifié :
```bash
[...]
nameserver 192.168.56.103
[...] 

# Après avoir quitter vi
sudo chattr +i /etc/resolv.conf
```

On redémarre le service `network` :
```bash
sudo systemctl restart network
```

On peut maintenant tester notre serveur DNS :
```bash
dig masterdns.projetinfra.net

nslookup projetinfra.net
```
On obtient :
```bash
[hboueix@masterdns ~]$ dig masterdns.projetinfra.net

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-9.P2.el7 <<>> masterdns.projetinfra.net
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 52650
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;masterdns.projetinfra.net.	IN	A

;; ANSWER SECTION:
masterdns.projetinfra.net. 86400 IN	A	192.168.56.103

;; AUTHORITY SECTION:
projetinfra.net.	86400	IN	NS	masterdns.projetinfra.net.

;; Query time: 0 msec
;; SERVER: 192.168.56.103#53(192.168.56.103)
;; WHEN: mar. févr. 25 23:07:04 CET 2020
;; MSG SIZE  rcvd: 84


[hboueix@masterdns ~]$ nslookup projetinfra.net

Server:		192.168.56.103
Address:	192.168.56.103#53

Name:	projetinfra.net
Address: 192.168.56.102
Name:	projetinfra.net
Address: 192.168.56.103
Name:	projetinfra.net
Address: 192.168.56.101
Name:	projetinfra.net
Address: 192.168.56.104
```

Pour terminer, on peut modifier son FQDN dans `/etc/hostname` :
```bash
sudi vi /etc/hostname

# On y mettra
masterdns.projetinfra.net
```