# ProjetInfra-b2

## Présentation

L'objectif de notre projet est de monter une infrastructure réseau qui délivre un service de cloud via l'application Nextcloud, un service d'annuaire LDAP avec un serveur FreeIPA et une résolution DNS avec un serveur DNS local, tout cela répartit sur 3 VMs.  
Une 4e VM sera dédiée à la backup de nos données Nextcloud via BorgBackup et une réplication du serveur FreeIPA.  
  
Notre application Nextcloud pourra enregistrer automatiquement les utilisateurs de l'annuaire LDAP grâce à une application que l'on pourra installer via l'interface web administrateur.
  
Les 4 VMs sont sous CentOS 7. Les liens ci-dessous présenteront les différentes étapes à réaliser pour mener à bien ce projet. Il est important de commencer par configurer le serveur DNS et de terminer par la VM de backup/réplica.

## Installation des VMs

- [Serveur DNS](https://gitlab.com/hboueix/projetinfra-b2/-/blob/master/Serveur%20DNS/README.md)
- [Nextcloud](https://gitlab.com/hboueix/projetinfra-b2/-/blob/master/Nextcloud%20-%20FreeIPA/README.md)
- [FreeIPA](https://gitlab.com/hboueix/projetinfra-b2/-/blob/master/FreeIPA/README.md)
- [BorgBackup / Replica](https://gitlab.com/hboueix/projetinfra-b2/-/blob/master/BorgBackup%20-%20Replica/README.md)


