# BorgBackup / Replica FreeIPA

On va commencer par configurer les DNS :
```bash
sudo vi /etc/resolv.conf
```

On rajoute une ligne juste avant celle contenant `nameserver` :
```bash
[...]
nameserver 192.168.56.103 # IP serveur DNS local
[...]
```

On va ensuite rendre le fichier *imutable* pour qu'il ne soit pas modifié :
```bash
sudo chattr +i /etc/resolv.conf
```

On modifie aussi notre FQDN dans le fichier `/etc/hostname` :
```bash
borgreplica.projetinfra.net
```

Et on n'oublie pas de mettre SELinux en mode *permissive* :
```bash
sudo setenforce 0
sudo vi /etc/selinux/config #SELINUX=enforcing -> SELINUX=permissive

# Pour vérifier
sestatus
```

## Replica FreeIPA

On va vérifier que notre FQDN est bien configuré et qu'il se résout bien avec `dig` :
```bash
# Si dig n'est pas installé
sudo yum -y install bind-utils

dig +short borgreplica.projetinfra.net A
# Sortie #
192.168.56.102
##########

dig +short -x 192.168.56.102
# Sortie #
borgreplica.projetinfra.net.
##########
```
Rajoutez cette ligne dans le fichier `/etc/hosts` :
```bash
192.168.56.102 borgreplica.projetinfra.net borgreplica
```

On va ouvrir les ports suivants dans le firewall :
- Ports TCP :
    - 80, 443 : HTTP/HTTPS
    - 389, 636 : LDAP/LDAPS
    - 88, 464 : kerberos
    - 53 : Bind -- Si besoin (DNS)
- Ports UDP :
    - 88, 464 : kerberos
    - 123 : ntp
    - 53 : Bind -- Si besoin (DNS)
```bash
sudo firewall-cmd --permanent --add-port={80/tcp,443/tcp,389/tcp,636/tcp,88/tcp,464/tcp,88/udp,464/udp,123/udp} # Rajoutez 53/tcp et 53/udp si besoin (DNS)
sudo firewall-cmd --reload
```

Nos machines virtuelles risquent de manquer rapidement d’entropie pour les opérations de chiffrement de FreeiIPA, il faut dans ce cas installer et activer rng-tools :
```bash
sudo yum -y install rng-tools
sudo systemctl start rngd && sudo systemctl enable rngd
```

On peut maintenant installer FreeIPA :
```bash
sudo yum -y install ipa-server

# Si besoin d'un serveur dns
sudo yum -y install ipa-server-dns
```

Nous allons à présent rejoindre le domaine FreeIPA en tant que simple client :
```bash
sudo ipa-client-install -U --domain=projetinfra.net --realm=PROJETINFRA.NET --server=freeipa.projetinfra.net --mkhomedir -p admin -w MotDePasse2
```
Explication des options :
- U : mode unattended, sans intervention de l’utilisateur (à désactiver en cas de soucis)
- domain= : domaine à rejoindre
- realm= : realm à rejoindre
- server= : serveur à utiliser par le client
- mkhomedir : créer automatiquement un dossier dans /home quand un nouvel utilisateur se connectera
- p : utilisateur autorisé à intégrer des machines ici admin
- w : mot de passe admin freeipa définit au début

Si tout c'est bien passé, vous devriez voir `borgreplica.projetinfra.net` sur l'interface web du serveur FreeIPA, dans la partie *"Hosts"*.

Sur notre serveur **`freeipa`**, on va ajouter `borgreplica` comme *"ipaserver"* :
```bash
[hboueix@freeipa ~]$ ipa hostgroup-add-member ipaservers --hosts borgreplica.projetinfra.net
```

Maintenant que notre serveur `borgreplica` est membre du domaine et est considéré comme un serveur ipa, nous allons pouvoir lancer l'installation du module de réplication :
```bash
sudo ipa-replica-install # --setup-ca --setup-dns --auto-forwarders --auto-reverse
# Options à rajouter si besoin de dns
```

On va installer un certificat d'autorité (CA) pour terminer (si vous n'avez pas utilisé l'option *--setup-ca*) :
```bash
sudo ipa-ca-install -p MotDePasse1
```

À la fin de l’installation, vérifiez en console et sur l’interface web que tout est ok, dans IPA Server -> Topology Graph :
```bash
[hboueix@borgreplica ~]$ kinit admin
Password for admin@PROJETINFRA.NET: # MotDePasse2

[hboueix@borgreplica ~]$ ipa user-find admin
--------------
1 user matched
--------------
  User login: admin
  Last name: Administrator
  Home directory: /home/admin
  Login shell: /bin/bash
  Principal alias: admin@PROJETINFRA.NET
  UID: 1842000000
  GID: 1842000000
  Account disabled: False
----------------------------
Number of entries returned 1
----------------------------
```
![Topology Graph](./img/topology-graph.png)

## BorgBackup

Tout d'abord on va devoir installer le dépôt source des packages EPEL, on pourra ensuite installer `borgbackup`:
```bash
sudo yum -y install epel-release

sudo yum -y install borgbackup
```

On va tenter de configurer BorgBackup pour qu'il sauvegarde nos données Nextcloud contenues sur notre serveur `nextcloud` en local sur cette machine `borgreplica` (pull).  
Sachant que BorgBackup est généralement utilisé pour faire l'inverse, soit sauvegarder des données locales sur un serveur distant (push), on va regarder du côté des 'Issues' Github ([document pull-like operation](https://github.com/borgbackup/borg/issues/900#issuecomment-499656737)) sur le dépôt du projet.


