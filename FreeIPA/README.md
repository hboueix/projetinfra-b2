# FreeIPA

On va commencer par configurer les DNS :
```bash
sudo vi /etc/resolv.conf
```

On rajoute une ligne juste avant celle contenant `nameserver` :
```bash
[...]
nameserver 192.168.56.103 # IP serveur DNS local
[...]
```

On va ensuite rendre le fichier *imutable* pour qu'il ne soit pas modifié :
```bash
sudo chattr +i /etc/resolv.conf
```

On modifie aussi notre FQDN dans le fichier `/etc/hostname` :
```bash
freeipa.projetinfra.net
```

Et on n'oublie pas de mettre SELinux en mode *permissive* :
```bash
sudo setenforce 0
sudo vi /etc/selinux/config #SELINUX=enforcing -> SELINUX=permissive

# Pour vérifier
sestatus
```

On va vérifier que notre FQDN est bien configuré et qu'il se résout bien avec `dig` :
```bash
# Si dig n'est pas installé
sudo yum -y install bind-utils

dig +short freeipa.projetinfra.net A
# Sortie #
192.168.56.104
##########

dig +short -x 192.168.56.104
# Sortie #
freeipa.projetinfra.net.
##########
```
Rajoutez cette ligne dans le fichier `/etc/hosts` :
```bash
192.168.56.104 freeipa.projetinfra.net freeipa
```

On va ouvrir les ports suivants dans le firewall :
- Ports TCP :
    - 80, 443 : HTTP/HTTPS
    - 389, 636 : LDAP/LDAPS
    - 88, 464 : kerberos
    - 53 : Bind -- Si besoin (DNS)
- Ports UDP :
    - 88, 464 : kerberos
    - 123 : ntp
    - 53 : Bind -- Si besoin (DNS)
```bash
sudo firewall-cmd --permanent --add-port={80/tcp,443/tcp,389/tcp,636/tcp,88/tcp,464/tcp,88/udp,464/udp,123/udp} # Rajoutez 53/tcp et 53/udp si besoin (DNS)
sudo firewall-cmd --reload
```

Nos machines virtuelles risquent de manquer rapidement d’entropie pour les opérations de chiffrement de FreeiIPA, il faut dans ce cas installer et activer rng-tools :
```bash
sudo yum -y install rng-tools
sudo systemctl start rngd && sudo systemctl enable rngd
```

On peut maintenant installer FreeIPA :
```bash
sudo yum -y install ipa-server

# Si besoin d'un serveur dns
sudo yum -y install ipa-server-dns
```

On va installer le serveur directement sur notre domaine :
```bash
sudo ipa-server-install -U -p MotDePasse1 -a MotDePasse2 --ip-address=192.168.56.104 -n projetinfra.net -r PROJETINFRA.NET --hostname=freeipa.projetinfra.net 
# On aurait pu rajouter --setup-dns --auto-forwarders --auto-reverse
```
Explications des options :
- U : mode unattended, sans intervention de l’utilisateur (à désactiver en cas de soucis)
- p MotDePasse1 : Mot de passe du directory manager, admin ultime côté LDAP
- a MotDePasse2 : Mot de passe de l’administrateur freeipa/kerberos
- ip-address= : l’ip de votre serveur
- n : le domaine géré par freeipa
- r : le realm kerberos, par convention le nom du domaine en majuscule
- hostname= : fqdn du serveur
- setup-dns : freeipa intègre la gestion de son propre dns si vous n’en possédez pas. Il est possible ne pas utiliser l’option pour gérer votre propre DNS
- auto-forwarders : création automatique des forwarders en se basant sur le fichier /etc/resolv.conf du serveur, pour la résolution des noms internet
- auto-reverse : création automatique de la zone reverse DNS
Les trois dernières options ne sont pas nécessaires dans notre cas puisque nous avons déjà configurer un serveur DNS local.

Étant donné qu'on a choisi de gérer nos DNS via un serveur en local, il faut bien noter cette ligne à la fin de l'installtion :  
*"Please add records in this file to your DNS system: /tmp/ipa.system.records.kw3tP_.db"*

On rajoute donc les enregistrements contenus dans ce fichier dans notre DNS, dans le fichier `/var/named/forward.projetinfra` :
```bash
[hboueix@freeipa ~]$ sudo cat /tmp/ipa.system.records.kw3tP_.db

_kerberos-master._tcp.projetinfra.net. 86400 IN SRV 0 100 88 freeipa.projetinfra.net.
_kerberos-master._udp.projetinfra.net. 86400 IN SRV 0 100 88 freeipa.projetinfra.net.
_kerberos._tcp.projetinfra.net. 86400 IN SRV 0 100 88 freeipa.projetinfra.net.
_kerberos._udp.projetinfra.net. 86400 IN SRV 0 100 88 freeipa.projetinfra.net.
_kerberos.projetinfra.net. 86400 IN TXT "PROJETINFRA.NET"
_kpasswd._tcp.projetinfra.net. 86400 IN SRV 0 100 464 freeipa.projetinfra.net.
_kpasswd._udp.projetinfra.net. 86400 IN SRV 0 100 464 freeipa.projetinfra.net.
_ldap._tcp.projetinfra.net. 86400 IN SRV 0 100 389 freeipa.projetinfra.net.
_ntp._udp.projetinfra.net. 86400 IN SRV 0 100 123 freeipa.projetinfra.net.
ipa-ca.projetinfra.net. 86400 IN A 192.168.56.104

# Sur notre serveur DNS
sudo vi /var/named/forward.projetinfra
```

Une fois les enregistrements ajoutés, vérifiez qu’ils sont bien pris en compte :
```bash
[hboueix@freeipa ~]$ dig +short srv _kerberos-master._tcp.projetinfra.net

0 100 88 freeipa.projetinfra.net.
```

On peut désormais s'assurer du bon fonctionnement du serveur avec la commande `kinit` et `ipa user-find` :
```bash
[hboueix@freeipa ~]$ kinit admin
Password for admin@PROJETINFRA.NET: 
kinit: Password incorrect while getting initial credentials
[hboueix@freeipa ~]$ kinit admin
Password for admin@PROJETINFRA.NET: # MotDePasse2

[hboueix@freeipa ~]$ ipa user-find admin
--------------
1 user matched
--------------
  User login: admin
  Last name: Administrator
  Home directory: /home/admin
  Login shell: /bin/bash
  Principal alias: admin@PROJETINFRA.NET
  UID: 1842000000
  GID: 1842000000
  Account disabled: False
----------------------------
Number of entries returned 1
----------------------------
```

On peut également accèder à l'interface web sur *https://freeipa.projetinfra.net* et se connecter avec l'utilisateur `admin` que l'on a créé avec la commande `kinit`.

![First connexion LDAP](./img/first-connexion-ldap.png)